#include <fontconfig/fontconfig.h>
#include <stdio.h>

int main(int argc, char **argv) {
  printf("Hello %s, from fontconfig %d!", argv[1], FcGetVersion());
  return 0;
}
